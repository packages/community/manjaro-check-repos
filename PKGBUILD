# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Stefano Capitani <stefano@manjaro.org>

pkgname=manjaro-check-repos
pkgver=0.9.10
pkgrel=2
pkgdesc="Manjaro Multi-Branch Navigator"
arch=('any')
url="https://gitlab.manjaro.org/applications/manjaro-check-repos"
license=('GPL-3.0-or-later')
depends=(
  'gtk3'
  'pacman'
  'polkit'
  'pyalpm'
  'python-gobject'
  'tree'
)
makedepends=('git')
backup=("etc/${pkgname}/manjaro.conf"
        "etc/${pkgname}/archlinux.conf")
source=("git+https://gitlab.manjaro.org/applications/manjaro-check-repos.git#tag=$pkgver")
sha256sums=('b382afe3b9ae9748a0433fc11502d0e1b09d4cdbfff247cd7f582b3dd690588a')

package() {
  cd "${pkgname}"
  install -Dm644 config/99-mbn.rules -t "${pkgdir}/usr/lib/polkit-1/rules.d/"
  install -Dm644 config/*.conf -t "${pkgdir}/etc/${pkgname}/"
  install -Dm644 config/*.policy -t "${pkgdir}/usr/share/polkit-1/actions/"
  install -Dm644 config/*.desktop -t "${pkgdir}/usr/share/applications/"
  install -Dm644 config/mbn.svg -t "${pkgdir}/usr/share/icons/hicolor/scalable/apps/"

  install -d "${pkgdir}/usr/share/${pkgname}"
  cp -r mbranch/* "${pkgdir}/usr/share/${pkgname}/"

  install -Dm755 mbn.sh "${pkgdir}/usr/bin/mbn"
  install -m755 mbn-gui.sh "${pkgdir}/usr/bin/mbn-gui"

  # Compile Python bytecode:
  python -m compileall -d /usr/share "${pkgdir}/usr/share"
  python -O -m compileall -d /usr/share "${pkgdir}/usr/share"
}
